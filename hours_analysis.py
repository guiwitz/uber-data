# local UTC
# 2019-03-31 01:02:52+00	2019-03-31 00:02:52+00
# 2019-03-31 17:52:31+00	2019-03-31 15:52:31+00
# UTC local
# 2019-10-27 00:00:00+00	2019-10-27 02:00:00+00
# 2019-10-27 01:00:00+00	2019-10-27 02:00:00+00
from datetime import datetime

summer_end = datetime.strptime("2019-10-27 02:00:00+00", "%Y-%m-%d %H:%M:%S+00")
summer_start = datetime.strptime("2019-03-31 03:00:00+00", "%Y-%m-%d %H:%M:%S+00")

day0 = datetime.strptime("2011-01-03 00:00:00+00", "%Y-%m-%d %H:%M:%S+00")

from datetime import datetime, timedelta
def summer_time(date):
    # Returns true if we are during summer time
    if summer_start <= date and date <= summer_end: 
        return True
    else:
        return False
    
def local_time(datestring, local=True):
    # returns the datestring as a date object, formulated in local time if local is True, otherwise in UTC
    tmp = datetime.strptime(datestring, "%Y-%m-%d %H:%M:%S+00")
    if local:
        return tmp
    else:
        if summer_time(tmp):
            return tmp - timedelta(hours=2)
        else:
            return tmp - timedelta(hours=1)

def week_index(datestring):
    return int((local_time(datestring) - day0).days/7.)+1

def monday_string(index):
    return (day0+timedelta(days=index*7-7)).strftime("%a %d.%m.%Y")
    
files = [\
"01 - Driver Profile Data.csv", 
"02 - Driver Lifetime Trips.csv",
         # request_timestamp_local
         # request_timestamp_utc
"04 - Driver GPS data.csv",
"05 - Driver device data.csv",
"06 - Driver Online Offline.csv",
         # begin_timestamp_local
         # end_timestamp_local
"07 - Driver App Restrictions.csv",
"09 - Driver Dispatches Offered and Accepted.csv"
         # start_timestamp_utc
         # end_timestamp_utc
         # start_timestamp_local
         # end_timestamp_local
]

def read_rows(driver, filename):
    import csv
    csvfile = open(driver + filename, "r")
    rowreader = csv.DictReader(csvfile)
    return rowreader

def read_date(datestring):
    from datetime import datetime
    # 2019-02-22 12:45:40+00
    return datetime.strptime(datestring, "%Y-%m-%d %H:%M:%S+00")

def distance_pts(coords_1, coords_2):
    from geopy import distance as geodist
    return geodist.distance(coords_1, coords_2).km

def french_system(hours):
    # computes overtime hours in the French system
    extra = max(hours - 35, 0)
    extra8 = max(extra - 8, 0)
    return extra/4. + extra8 / 4.
    

def weekly_hours(driver, filename="output"):
    import collections

    transition_counter = collections.Counter()
    duration_counter = collections.Counter()
    distance_counter = collections.Counter()
    duration_counters = [collections.Counter() for i in range(1000)]

    for i, row in enumerate(read_rows(driver, "06 - Driver Online Offline.csv")):
        try:
            value_seconds = int(row["duration_seconds"])/3600.
            duration_counter[row["status"]] += value_seconds
            duration_counters[week_index(row["begin_timestamp"])][row["status"]] += value_seconds
        except:
            pass

    #old_status = "open"
    #for i, row in enumerate(read_rows("06 - Driver Online Offline.csv")):
    #    new_status = row["status"]
    #    transition_counter[(old_status, new_status)] += 1
    #    old_status = new_status
    #    begin = row["begin_timestamp"]
    #    end = row["end_timestamp"]    
    #    try:
    #        diff = ((read_date(end)-read_date(begin)).total_seconds()-int(row["duration_seconds"]))
    #        if diff > 1: 
    #            print(i, "LARGE")
    #    except:
    #        print(i, row["status"])

    for i, row in enumerate(read_rows(driver, "06 - Driver Online Offline.csv")):
        try:
            p1 = (row["begin_lat"], row["begin_lng"])
            p2 = (row["end_lat"], row["end_lng"])
            distance_counter[row["status"]] += distance_pts(p1, p2)
        except:
            pass
    entry = lambda value, just: ("%.2f" % value).center(just)

    from prettytable import PrettyTable


    title = """
Legende du fichier ci-dessus

Heures de travail, par semaine commencant le lundi
==================================================
"open": chauffeur en attente de course
"en route": chauffeur en approche
"on trip": chauffeur conduit un passager
"travail": somme "on trip" + "en route" + "open"
"heures supp": 100 de l'activite entre 0 et 35 heures, 125% de 35 heures a 43, 150% au-dela
"""
    import csv
    csvfile = open(filename+".csv", "w")
    fieldnames = ["Lundi", "'open'", "'en route'", "'on trip'", "travail", "heures supp"]
    hours_worked = PrettyTable()
    hours_worked.field_names = fieldnames
    
    writer=csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

    total_open = 0
    total_en_route = 0
    total_on_trip = 0
    total_work = 0
    total_extra = 0
    for i, dc in enumerate(duration_counters):
        is_open = dc["open"]
        en_route = dc["en route"]
        on_trip = dc["on trip"]
        work = is_open + en_route + on_trip
        extra = (french_system(work))
    
        if work:
            row_string = [monday_string(i).center(16), entry(is_open,10), entry(en_route,10), entry(on_trip, 10), entry(work, 10), entry(extra, 10)]
            hours_worked.add_row(row_string)
            writer.writerow(dict(zip(fieldnames, row_string)))
    
        total_open += is_open
        total_en_route += en_route
        total_on_trip += on_trip
        total_work += work
        total_extra += extra

        
    row_string = ["Total", entry(total_open, 10), entry(total_en_route, 10), entry(total_on_trip, 10), entry(total_work, 10), entry(total_extra, 10)]
    hours_worked.add_row(row_string)
    writer.writerow(dict(zip(fieldnames, row_string)))
    csvfile.close()
    


    from IPython.display import display, FileLink, HTML
    
    display(HTML("<h2>Chargement du fichier</h2>"))
    

    local_file = FileLink(filename+".csv", result_html_prefix="Telechargez directement votre fichier: ")
    display(local_file)
    
    #display(HTML("<h2>Visualisez directement</h2>"))    
    
    #print(title+hours_worked.get_string())

def hours_09(driver):
    import collections

    time_counter = collections.defaultdict(float)

    for i, row in enumerate(read_rows(driver, "09 - Driver Dispatches Offered and Accepted.csv")):
        try:
            time_counter["online"] += float(row["minutes_online"])/60.
            time_counter["on trip"] += float(row["minutes_on_trip"])/60.
            time_counter["active"] += float(row["minutes_active"])/60.
        except:
            print("error")
    
    time_counter["open"] = time_counter["online"] - time_counter["active"]
    time_counter["en route"] = time_counter["active"] - time_counter["on trip"]
    return time_counter


# Attempt at calcuating the cost:

# for i, row in enumerate(read_rows("02 - Driver Lifetime Trips.csv")):
#     if row["status"] == "completed":
#         try:
#             # print(row["base_fare_local"], row["uuid"])
#             original = float(row["original_fare_local"])
#             base = float(row["base_fare_local"])
#             surge_fare = float(row["surge_fare_local"])
#             mile = float(row["per_mile_fare_local"]) * float(row["trip_distance_miles"])
#             duration = float(row["per_minute_fare_local"]) * float(row["trip_duration_seconds"])/60.
#             toll = float(row["toll_amount_local"])
#             surge = float(row["surge_multiplier"])
#
#             print(row["uuid"], original - mile - duration - base - toll, surge, surge_fare)
#             # trip_distance_mile
#             # trip_duration_seconds
#             #    per_minute_fare_local
#             #    toll_amount_local
#             #    service_fee_local
#             #    fare_duration_minutes
#             #    wait_duration_minutes
#         except:
#             pass

